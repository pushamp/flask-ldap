*************
Setting it up
*************

Here’s an example of how Flask-LDAP is typically initialized and configured:

.. code-block:: python

	from flask import Flask
	from flask.ext.ldap import LDAP, login_required
	from flask.ext.pymongo import PyMongo
	app = Flask(__name__)
	app.debug = True

	app.config['LDAP_HOST'] = 'ldap.example.com'
	app.config['LDAP_DOMAIN'] = 'example.com'
	app.config['LDAP_SEARCH_BASE'] = 'OU=Domain Users,DC=example,DC=com'
	app.config['LDAP_LOGIN_VIEW'] = 'custom_login'

	app.config['MONGO_DBNAME'] = 'simpledb'
	mongo = PyMongo(app, config_prefix='MONGO')
	ldap = LDAP(app, mongo)
	app.secret_key = "welfhwdlhwdlfhwelfhwlehfwlehfelwehflwefwlehflwefhlwefhlewjfhwelfjhweflhweflhwel"
	app.add_url_rule('/login', 'login', ldap.login, methods=['GET', 'POST'])

If you not use MongoDB, you can initialization extension without MongoDB support (user profile can't be stored):

.. code-block:: python

   ldap = LDAP(app)

=====================
Configuration options
=====================

Flask-LDAP is configured via the standard Flask config API. Here are the available configuration options:

* **LDAP_HOST** - the hostname or IP address ldap server (Default: "127.0.0.1")
* **LDAP_PORT** - the port where ldap server is listening (Default: 389)
* **LDAP_SCHEMA** - the schema that should be used to reach the ldap server (Default: "ldap")
* **LDAP_DOMAIN** - domain name (Default: "example.com")
* **LDAP_SEARCH_BASE** - setting the search base (Default: "OU=Users,DC=example,DC=com")
* **LDAP_REQUIRED_GROUP** - If specified, the authentication must succeed AND the user must be part of this security group. (Ex: "CN=security_group_name,OU=Groups,DC=example,DC=com" )
* **LDAP_LOGIN_VIEW** - function name for auth login form (Default: "login")
* **LDAP_LOGIN_TEMPLATE** - name of template for rendering auth form (Default: "login.html")
* **LDAP_SUCCESS_REDIRECT** - function name for redirect after success login (Default: 'index')
* **LDAP_PROFILE_KEY** - name of the attribute in the user's account from LDAP to save as '_id' to MongoDB (Default: 'sAMAccountName')
* **LDAP_AVATAR_LOC** - path of user's avatar from LDAP. If LDAP_PROFILE_KEY is defined then its value is used as name of image file, else value of userPrincipalName is used.